#!/usr/bin/env node

var fs = require('fs'),
    esprima = require('esprima');

function analyzeTest(verbs, block) {
    block.body.forEach(function (statement) {
        var callee;
        if (statement.type === 'ExpressionStatement') {
            if (statement.expression.type === 'CallExpression') {
                if (statement.expression.callee.type === 'Identifier') {
                    callee = statement.expression.callee;
                    if (statement.expression.arguments.length > 0 ){
                        if (verbs.testLevel.indexOf(callee.name) < 0) {
                            verbs.testLevel.push(callee.name);
                        }
                    }
                }
            }
        }
    });
}

function analyzeTop(program) {
    var verbs = {
        topLevel: [],
        testLevel: []
    }
    program.body.forEach(function (statement) {
        var callee;
        if (statement.type === 'ExpressionStatement') {
            if (statement.expression.type === 'CallExpression') {
                if (statement.expression.callee.type === 'Identifier') {
                    callee = statement.expression.callee;
                    if (statement.expression.arguments.length > 0 ){
                        if (verbs.topLevel.indexOf(callee.name) < 0) {
                            verbs.topLevel.push(callee.name);
                        }
                        statement.expression.arguments.forEach(function (arg) {
                            if (arg.type === 'FunctionExpression') {
                                analyzeTest(verbs, arg.body);
                            }
                        });
                    }
                }
            }
        }
    });
    return verbs;
}

function decide(verbs) {
    var lib = 'Unknown';
    verbs.topLevel.forEach(function (v) {
        if (' module test asyncTest'.indexOf(v) > 0) {
            verbs.testLevel.forEach(function (t) {
                if (' equal expect strictEqual deepEqual throws'.indexOf(t) > 0) {
                    lib = 'qUnit';
                }
            });
        } else if (' describe'.indexOf(v) > 0) {
            verbs.testLevel.forEach(function (t) {
                if (' beforeEach afterEach describe it'.indexOf(t) > 0) {
                    lib = 'Jasmine';
                }
            });
        }
    });

    // For debugging:
    // console.log(verbs);

    console.log('Detected TDD/BDD library:', lib);
}

function check(filename) {
    var content, tree;
    try {
        content = fs.readFileSync(filename, 'utf-8');
        decide(analyzeTop(esprima.parse(content)));
    } catch (e) {
        console.error(e.toString());
        process.exit(1);
    }
}

if (process.argv.length === 2) {
    console.error('Usage: detect-testlib.js filename');
    process.exit(1);
}
check(process.argv[2]);
