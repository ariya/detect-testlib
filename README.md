This is an example of using [Esprima](http://esprima.org/) to automatically detect JavaScript TDD/BDD library (QUnit or Jasmine) being used in unit tests code.

Run the following:

```
npm install
node detect-testlib.js test/jquery-attributes.js
```

and try also:

```
node detect-testlib.js test/yeoman-env.js
```
